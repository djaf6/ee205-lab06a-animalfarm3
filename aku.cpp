///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/22/2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include "aku.hpp"

using namespace std;

namespace animalfarm {
   Aku::Aku (float newWeight, enum Color newColor, enum Gender newGender){
      weight = newWeight;
      species = "Katsuwonus pelamis";
      gender = newGender;
      scaleColor = newColor;
      favoriteTemp = 75;

}

void Aku::printInfo(){
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}
}
