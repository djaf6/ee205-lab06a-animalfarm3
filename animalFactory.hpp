//////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 06a - Animal Farm 3
/// 
/// @file animalFactory.hpp
/// @version 1.0
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief Lab06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date 03/24/2021
/////////////////////////////////////////////////////////////

#include "animal.hpp"

using namespace std;

namespace animalfarm {
   class animalFactory{
      public:
         static Animal* getRandomAnimal();
   };
}
