///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/22/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>
#include "animal.hpp"
#include "animalFactory.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   cout << "Welcome to Animal Farm 3" << endl;

   array <Animal*, 30> animalArray;
   animalArray.fill(NULL);

   for (int i = 0; i < 25; i++){
      animalArray[i] = animalFactory::getRandomAnimal();
   }

   cout << endl;
   cout << "Array of animals:" << endl;
   cout << boolalpha << "  Is it empty: " << animalArray.empty() << endl;
   cout << "  Number of elements: " << animalArray.size() << endl;
   cout << "  Max size: " << animalArray.max_size() << endl;

   for (Animal* animal : animalArray){
      if(animal == NULL){
         break;
      }

      cout << animal -> speak() << endl;
   }

   for (int i = 0; i < animalArray.size(); i++){

      delete animalArray[i];
   }

   list <Animal*> animalList;

   for (int i = 0; i < 25; i++){
      animalList.push_front(animalFactory::getRandomAnimal());
   }

   cout << endl;
   cout << "List of Animals:" << endl;
   cout << boolalpha << "  Is it empty: " <<animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;
   cout << "  Max size:  " << animalList.max_size() << endl;

   for (Animal* animal : animalList) {
      cout << animal -> speak () << endl;
   }

   while (!animalList.empty()){
      delete animalList.front();
      animalList.pop_front();
   }

   return 0;
}
