///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Destynee Fagaragan  <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/22/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "fish.hpp"

using namespace std; 

namespace animalfarm{
   class Aku : public Fish {
      public:
         float weight;
         Aku (float newWeight, enum Color newColor, enum Gender newGender);

         void printInfo();
   };
}
