///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/22/2020
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animal.hpp"

namespace animalfarm{
   class Bird : public Animal {
      public:
         enum Color featherColor;
         bool isMigratory;

         virtual const string speak();

         void printInfo();
   };

}
