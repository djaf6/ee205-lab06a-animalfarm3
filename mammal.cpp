///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file mammal.cpp
/// @version 1.0
///
/// Exports data about all mammals
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03/20/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 

#include "mammal.hpp"

using namespace std;

namespace animalfarm {
	
void Mammal::printInfo() {
	Animal::printInfo();
	cout << "   Hair Color = [" << colorName( hairColor ) << "]" << endl;
	cout << "   Gestation Period = [" << gestationPeriod << "]" << endl;
}

} // namespace animalfarm
